module.exports = function() {
    const firebase = require("firebase/app");
    const fs = require('fs')

    require("firebase/auth");
    require("firebase/firestore");

    const firebaseConfig = {
        apiKey: "XXX",
        authDomain: "XXX",
        databaseURL: "XXX",
        projectId: "XXX",
        storageBucket: "XXX",
        messagingSenderId: "XXX",
        appId: "XXX"
    }

    firebase.initializeApp(firebaseConfig)

    return firebase.firestore()
}