module.exports = async function (req, res, db) {
    console.log('CONFIRM_TINKOFF_PAYMENT: Entry point')
    let tinkoffData = req.body
    // let hasOrder = false
    if (tinkoffData.Status = 'AUTHORIZED') {
        console.log('CONFIRM_TINKOFF_PAYMENT: Entry point')
        try {
            console.log({tinkoffData})
            let collectionDoc = `order_${tinkoffData.OrderId}`
            // нашелся ордер, теперь надо его переместить со статусом authorized
            // fs.readFileSync(`./buffer/orders/non_status/order_${tinkoffData.OrderId}.json`)
            // hasOrder = true
    
            let orderDocRef = db
            .collection('opencart_order_buffer').doc('buffer')
            .collection('pending').doc(collectionDoc)
            let doc = await orderDocRef.get()
            let order = doc.data()
            order.tinkoff = {
                ...order.tinkoff,
                ...tinkoffData
            }
            await orderDocRef.delete()
            await db
                .collection('opencart_order_buffer').doc('buffer')
                .collection('authorized').doc(collectionDoc).set(order)
            res.send('OK')
        } catch(e) {
            // нет нужного ордера, нужно сделать отмену платежа
            console.log(e)
        }
    } else {
        res.send('ERROR')
    }
    

    // if (hasOrder) {
    //     let order = JSON.parse(fs.readFileSync(`./buffer/orders/non_status/order_${tinkoffData.OrderId}.json`))
    //     order.tinkoff.tinkoffStatus = tinkoffData.Status
    //     fs.unlinkSync(`./buffer/orders/non_status/order_${tinkoffData.OrderId}.json`)
    //     fs.writeFileSync(`./buffer/orders/authorized/order_${tinkoffData.OrderId}.json`, JSON.stringify(order, 0, 4))
    // }
}