module.exports = function (req, res, db) {
    const crypto = require('crypto')
    
    const opencartInput = req.body
    opencartInput.tinkoff = {
        tinkoffStatus: "WAITING_TINKOFF_PAYMENT_CONFIRMATION"
    }

    async function bufferOpencartOrder() {
        // буферизуем ордер до момента, когда придет статус от тинькова
        if (!opencartInput.test) {
            // fs.writeFileSync(`./buffer/orders/non_status/order_${opencartInput.order.order_id}.json`, JSON.stringify(opencartInput, 0, 4))
            opencartInput.order.date_created = Date.now()
            opencartInput.order.order_unique_hash = `order_${crypto.randomBytes(20).toString('hex')}`
            for (let product of opencartInput.products) {
                product.is_processed = false
                product.unique_hash = crypto.randomBytes(20).toString('hex')
            }
            await db
                .collection('opencart_order_buffer').doc('buffer')
                .collection('pending').doc(`order_${opencartInput.order.order_id}`)
                .set(opencartInput)
            console.log(`API_NEWORDER: saved new order: db:opencart_order_buffer=buffer/pending=${opencartInput.order.order_id}`)
        } else {
            for (let product of opencartInput.products) {
                product.is_processed = false
                product.unique_hash = crypto.randomBytes(25).toString('hex')
            }
            console.log('API_NEWORDER: new order,', opencartInput)
            console.log('API_NEWORDER: test request without saving order')
        }
    }
    
    bufferOpencartOrder()     
    res.send('OK')    
}