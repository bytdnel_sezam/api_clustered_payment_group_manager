module.exports = function (req, res) {
    const fs = require('fs')
    const nodemailer = require('nodemailer');
    let getMailHtml = require('../functions/getMailHTML')

    
    let _credentials = {
        services: {
            gmail: {
                itself: 'gmail',
                email: 'danielbyta.student@gmail.com',
                password: 'ivrit007'
            },
            own: {
                host: 'smtp.yandex.ru',
                port: 587,
                auth: {
                    user: 'main@sezam.shop',
                    pass: 'N0n3Fruit$?8+'
                }
            }
        }
    }

    let transporter = nodemailer.createTransport({
        host: _credentials.services.own.host,
        port: _credentials.services.own.port,
        secure: false,
        auth: {
            user: _credentials.services.own.auth.user,
            pass: _credentials.services.own.auth.pass
        }
    })

    let clientMailSubject = req.body.is_acception ? 'Акцепт договора с Sezam' : 'Начало сотрудничества с Sezam'
    let b2bMailSubject = req.body.is_acception ? 'API: Договор акцептован' : 'API: Новая заявка на сотрудничество'

    let [clientHtml, b2bHtml] = getMailHtml(req.body)

    try {
        transporter.sendMail({
            from: `"SezamGroup Bot" <${_credentials.services.own.auth.user}>`,
            to: `${req.body.email}`,
            subject: `no-reply | ${clientMailSubject}`,
            text: "",
            html: clientHtml
        }, (err) => {
            if (err) res.send({success: false, payload: err})
            else {
                res.send({success: true})
            }
        })

        transporter.sendMail({
            from: `"SezamGroup Bot" <${_credentials.services.own.auth.user}>`,
            to: `vendor@sezam.shop, sphilippov4567@gmail.com`,
            subject: `no-reply | ${b2bMailSubject}`,
            text: "",
            html: b2bHtml
        }, (err) => {
            if (err) res.send({success: false, payload: err})
            else {
                res.send({success: true})
            }
        })
    } catch(e) {
        res.send({success: false, payload: e})
    }
   
}
