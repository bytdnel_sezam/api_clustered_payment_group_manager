module.exports = function (req, res) {
    let filenames = fs.readdirSync(`./${req.params.filename.split(':').join('/')}`)
    res.send(JSON.stringify(filenames, 0, 4))
}