module.exports = function(obj) {
    const fs = require('fs')
    let styles = fs.readFileSync('./html/mailcss.txt').toString()
    console.log({obj})
    let data = obj.is_acception ? 
    //private
    `
    <div mc:edit="body" style=" text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 15px; margin-bottom: 0; color: #5f5f5f; line-height: 135%;">
    Адрес электронной почты:
    ${obj.email}
    </div>
    <div mc:edit="body" style=" text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 15px; margin-bottom: 0; color: #5f5f5f; line-height: 135%;">
    Тип ЮЛ: ${obj.contractPayload.type_entity}
    </div>
    <div mc:edit="body" style=" text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 15px; margin-bottom: 0; color: #5f5f5f; line-height: 135%;">
    Наименование организации:
    ${obj.contractPayload.name_organisation}
    ${obj.contractPayload.name_organisation_ooo}
    </div>
    <div mc:edit="body" style=" text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 15px; margin-bottom: 0; color: #5f5f5f; line-height: 135%;">
    Юридический адрес: 
    ${obj.contractPayload.legal_address}
    ${obj.contractPayload.legal_address_ooo}
    </div>
    <div mc:edit="body" style=" text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 15px; margin-bottom: 0; color: #5f5f5f; line-height: 135%;">
    КПП: ${obj.contractPayload.kpp}
    </div>
    <div mc:edit="body" style=" text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 15px; margin-bottom: 0; color: #5f5f5f; line-height: 135%;">
    ИНН: 
    ${obj.contractPayload.inn}
    ${obj.contractPayload.inn_ooo}
    </div>
    <div mc:edit="body" style=" text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 15px; margin-bottom: 0; color: #5f5f5f; line-height: 135%;">
    ОГРН/ОГРНИП: ${obj.contractPayload.ogrnyp||obj.contractPayload.ogrn}
    </div>
    <div mc:edit="body" style=" text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 15px; margin-bottom: 0; color: #5f5f5f; line-height: 135%;">
    Р/С: ${obj.contractPayload.rs}
    </div>
    <div mc:edit="body" style=" text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 15px; margin-bottom: 0; color: #5f5f5f; line-height: 135%;">
    Банк: ${obj.contractPayload.name_bank}
    </div>
    <div mc:edit="body" style=" text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 15px; margin-bottom: 0; color: #5f5f5f; line-height: 135%;">
    БИК Банка: ${obj.contractPayload.bic_bank}
    </div>
    <div mc:edit="body" style=" text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 15px; margin-bottom: 0; color: #5f5f5f; line-height: 135%;">
    ИНН Банка: ${obj.contractPayload.inn_bank}
    </div>
    <div mc:edit="body" style=" text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 15px; margin-bottom: 0; color: #5f5f5f; line-height: 135%;">
    КОРР Счет Банка: ${obj.contractPayload.account_bank}
    </div>
    <div mc:edit="body" style=" text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 15px; margin-bottom: 0; color: #5f5f5f; line-height: 135%;">
    Юридический адрес банка: ${obj.contractPayload.legal_address_bank}
    </div>
    ` : 
    //public
    `
    <div mc:edit="body" style=" text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 15px; margin-bottom: 0; color: #5f5f5f; line-height: 135%;">
    Название компании:
    ${obj.contractPayload.name_organisation}
    </div>
    <div mc:edit="body" style=" text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 15px; margin-bottom: 0; color: #5f5f5f; line-height: 135%;">
    Тип ЮЛ: ${obj.contractPayload.type_entity}
    </div>
    <div mc:edit="body" style=" text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 15px; margin-bottom: 0; color: #5f5f5f; line-height: 135%;">
    ИНН компании:
    ${obj.contractPayload.inn}
    </div>
    <div mc:edit="body" style=" text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 15px; margin-bottom: 0; color: #5f5f5f; line-height: 135%;">
    Категории товаров:
    ${obj.contractPayload.category_product}
    </div>
    <div mc:edit="body" style=" text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 15px; margin-bottom: 0; color: #5f5f5f; line-height: 135%;">
    Адрес электронной почты:
    ${obj.email}
    </div>
    <div mc:edit="body" style=" text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 15px; margin-bottom: 0; color: #5f5f5f; line-height: 135%;">
    Номер телефона:
    ${obj.contractPayload.number_phone}
    </div>
    <div mc:edit="body" style=" text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 15px; margin-bottom: 0; color: #5f5f5f; line-height: 135%;">
    ФИО контактного лица:
    ${obj.contractPayload.fio}
    </div>
    `

    let clentHtml = `
      <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
      <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
          <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
          <meta name="format-detection" content="telephone=no" />
          <!-- disable auto telephone linking in iOS -->
          <title>Sezam group thank you text</title>
          ${styles}
        </head>
        <body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
          <center style="background-color: #e1e1e1">
            <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%"id="bodyTable" style=" table-layout: fixed; max-width: 100% !important; width: 100% !important; min-width: 100% !important;">
              <tr>
                <td align="center" valign="top" id="bodyCell">
                  <table bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="500" id="emailBody">
                    <tr>
                      <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="color: #ffffff" bgcolor="#3498db">
                          <tr>
                            <td align="center" valign="top">
                              <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                                <tr>
                                  <td align="center" valign="top" width="500" class="flexibleContainerCell">
                                    <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                      <tr>
                                        <td align="center" valign="top" class="textContent">
                                          <h1 style=" color: #ffffff; line-height: 100%; font-family: Helvetica, Arial, sans-serif; font-size: 30px; font-weight: normal; margin-bottom: 5px; text-align: center;">
                                          ${obj.is_acception ? 
                                            'Настоящее письмо-уведомление подтверждает присоединение (акцептование) Вашей компании к Договору Оферты Sezam с ИП Сушенцев Данил Михайлович.'
                                            :
                                            'Спасибо за проявленный интерес.'
                                          }
                                          </h1>
                                          <h2 style="text-align: center; font-weight: normal; font-family: Helvetica, Arial, sans-serif; font-size: 23px; margin-bottom: 10px; color: #205478; line-height: 135%; ">
                                          ${obj.is_acception ? 
                                            'Для акцепта Вами были внесены следующие данные:' 
                                            :
                                            'Команда Sezam свяжется с вами в ближайшее время!'
                                          }
                                          </h2>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
      
                    <tr>
                      <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F8F8F8">
                          <tr>
                            <td align="center" valign="top">
                              <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                                <tr>
                                  <td align="center" valign="top" width="500" class="flexibleContainerCell">
                                    <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                      <tr>
                                        <td align="center" valign="top">
                                          <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                              <td valign="top" class="textContent">
                                                <h3 mc:edit="header" style=" color: #5f5f5f; line-height: 125%; font-family: Helvetica, Arial, sans-serif; font-size: 20px; font-weight: normal; margin-top: 0; margin-bottom: 3px; text-align: left;">
                                                  Ваши данные:
                                                </h3>
                                                ${data}
                                              </td>
                                            </tr>
                                          </table>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
      
                    <tr>
                      <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tr>
                            <td align="center" valign="top">
                              <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                                <tr>
                                  <td align="center" valign="top" width="500" class="flexibleContainerCell">
                                    <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                      <tr>
                                        <td align="center" valign="top">
                                          <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                              <td align="center" valign="top" class="textContent">
                                                <div style="text-align: center;font-family: Helvetica, Arial, sans-serif; font-size: 15px; margin-bottom: 0; margin-top: 3px; color: #5f5f5f; line-height: 135%;">
                                                  С уважением, команда Sezam!
                                                </div>
                                                <img src="http://sezam.shop/image/catalog/logo/favicon%202.png" width="210" class="flexibleImage" style="max-width: 10%; text-align: center;" alt="Text" title="Text"/>
                                              </td>
                                            </tr>
                                          </table>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </center>
        </body>
  </html>`
    let b2bHtml = obj.is_acception ? 
    //private
    `
      <h3>${'Акцепт договора.'}</h3>
      <div><b>Данные:</b></div>
      <div>Адрес электронной почты: ${obj.email}</div>
      <div>Тип ЮЛ: ${obj.contractPayload.type_entity}</div>
      <div>Наименование организации: ${obj.contractPayload.name_organisation}${obj.contractPayload.name_organisation_ooo}</div>
      <div>Юридический адрес: ${obj.contractPayload.legal_address}${obj.contractPayload.legal_address_ooo}</div>
      <div>ИНН: ${obj.contractPayload.inn}${obj.contractPayload.inn_ooo}</div>
      <div>КПП: ${obj.contractPayload.kpp}</div>
      <div>ОГРН/ОГРНИП: ${obj.contractPayload.ogrnyp||obj.contractPayload.ogrn}</div>
      <div>Р/с: ${obj.contractPayload.rs}</div>
      <div>Банк: ${obj.contractPayload.name_bank}</div>
      <div>БИК банка: ${obj.contractPayload.bic_bank}</div>
      <div>ИНН банка: ${obj.contractPayload.inn_bank}</div>
      <div>Кор.счет банка: ${obj.contractPayload.account_bank}</div>
      <div>Юридический адрес банка: ${obj.contractPayload.legal_address_bank}</div>
      <p>Время отправки сообщения: ${new Date()}</p>
    ` : 
    //public
    `
      <h3>${'Заявка на сотрудничество с сайта.'}</h3>
      <div><b>Данные:</b></div>
      <div>Название компании: ${obj.contractPayload.name_organisation}</div>
      <div>Тип ЮЛ: ${obj.contractPayload.type_entity}</div>
      <div>ИНН компании: ${obj.contractPayload.inn}</div>
      <div>Категории товаров: ${obj.contractPayload.category_product}</div>
      <div>Адрес электронной почты: ${obj.email}</div>
      <div>Номер телефона: ${obj.contractPayload.number_phone}</div>
      <div>ФИО контактного лица: ${obj.contractPayload.fio}</div>
    `
    return [clentHtml, b2bHtml]
}
