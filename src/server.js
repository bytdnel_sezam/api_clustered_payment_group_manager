module.exports = function(definedPort = null, controllers, db) {
    const os = require('os')
    const compression = require('compression')
    const express = require('express')
    const bodyParser = require('body-parser')
    const cors = require('cors')
    const app = express()
    
    const port = definedPort || process.env.PORT
    
    app.use(compression())
    app.use(bodyParser.json())
    app.use(cors())

    app.all("/*", function (req, res, next) {

        res.header("Access-Control-Allow-Origin", req.headers.origin);
        res.header("Access-Control-Allow-Credentials",true);
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
        res.header('Access-Control-Allow-Headers', 'Content-Type,Accept,X-Access-Token,X-Key,Authorization,X-Requested-With,Origin,Access-Control-Allow-Origin,Access-Control-Allow-Credentials');
        if (req.method === 'OPTIONS') {
          res.status(200).end();
        } else {
          next();
        }
      });

    app.get('/', async (req, res) => {
        /*
        let snapshot = await db.collection('opencart_order_groups').doc('groups').collection('pending').get()
        let groupPromises = []
        snapshot.forEach(doc => groupPromises.push(new Promise((resolve, rej) => {
          resolve(doc.data())
        })))

        let groups = await Promise.all(groupPromises)
        res.send(groups)
        */
        res.send(`
        OPENCART GROUP API. 
        Heroku CPUs: ${JSON.stringify(os.cpus(), 0, 4)}
        CPUs count: ${os.cpus().length}
        `)
    })
    
    app.get('/getfile/:pathfile', controllers.getfile)
    app.get('/getfilenames/:filename', controllers.getfilenames)
    app.post('/api/newsezamcoop', controllers.newsezamcoop)
    app.post('/api/confirmpayment', (req, res, next) => controllers.confirmpayment(req, res, db))
    app.post('/api/neworder', (req, res, next) => controllers.neworder(req, res, db))

    
    app.listen(port, () => console.log(`listening localhost, port:${port}`))
}