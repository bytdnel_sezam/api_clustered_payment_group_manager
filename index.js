const cluster = require('cluster')
const os = require('os')
const getDatabase = require('./src/firebase.js')
const initializeServer = require('./src/server.js')
const initializeGroupManager = require('./intervals/GroupsManager.js')
const initializeConfirmationManager = require('./intervals/ConfirmationManager.js')
const initializeSendOrderManager = require('./intervals/SendOrderManager.js')
const serverCallbacks = {
    newsezamcoop: (req, res) => require('./src/request_callbacks/newsezamcoop.js')(req, res),
    getfile: (req, res) => require('./src/request_callbacks/getfile.js')(req, res),
    getfilenames: (req, res) => require('./src/request_callbacks/getfilenames.js')(req, res),
    confirmpayment : (req, res, db) => require('./src/request_callbacks/tinkoffpaymentstatus.js')(req, res, db),
    neworder : (req, res, db) => require('./src/request_callbacks/neworder.js')(req, res, db)
}

const db = getDatabase()

if (cluster.isMaster) {
    for (let i = 0; i<os.cpus().length-1; i++) {
        cluster.fork()
    }
    let time
    if (process.argv[2] == 'prod') {
        time = 1000*60*5
    } else {
        time = 1000*6
    }

    initializeGroupManager('./groups', time, db)
    initializeConfirmationManager('./buffer/orders', time*2, db)
    initializeSendOrderManager(db, time*4)

    cluster.on('exit', (worker) => {
        console.log('New process')
        cluster.fork()
    })
}

if (cluster.isWorker) {
    if (process.argv[2] == 'dev') {
        let port = 4000
        console.log('SERVER: initializing localhost server, port', port)
        initializeServer(port, serverCallbacks, db)
    } else {
        console.log('SERVER: initializing heroku server, port', process.env.PORT)
        initializeServer(process.env.PORT, serverCallbacks, db)
    }
}


