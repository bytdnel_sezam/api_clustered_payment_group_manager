const _DEFAULT_TIME = 1000*60*15

module.exports = function(db, TIME = _DEFAULT_TIME) {
    const axios = require('axios')
    const cheerio = require('cheerio')

    function getBoxberryInfo(html) {
        try {
            let $ = cheerio.load(html)
            let data = {
                boxberry_issue_point_name: $("#boxberry-issue_point-name").text(),
                boxberry_issue_point_addr_short: $("#boxberry-issue_point-addr-short").text(),
                boxberry_issue_point_phone: $("#boxberry-issue_point-phone").text(),
                data_boxberry_weigh: $("#boxberry-issue_point-link-prepaid").data("boxberryWeight"),
                data_boxberry_width: $("#boxberry-issue_point-link-prepaid").data("boxberryWidth"),
                data_boxberry_height: $("#boxberry-issue_point-link-prepaid").data("boxberryHeight"),
                data_boxberry_length: $("#boxberry-issue_point-link-prepaid").data("boxberryLength")
            }
            let toReturn = Object.entries(data).reduce((total, [key, value]) => {
                return total + `        - ${key}: ${value}\n`
            }, '')
            return toReturn
        } catch(e) {
            return '        [E]: noBoxberryData'
        }
    }
    function getRefundAmount(product) {
            if (product.price_type == 'group') {
                let endpoint_quantity
                product.firebase.quantity > 10 ? endpoint_quantity = 10 : endpoint_quantity = product.firebase.quantity
                return (
                    product.quantity*(endpoint_quantity*(product.single_price - product.group_price)/10)
                )
            } else {
                return 0
            }
    }
    async function sendOrderEmail(data) {
        const nodemailer = require('nodemailer');
        let _credentials = {
            services: {
                gmail: {
                    itself: 'gmail',
                    email: 'danielbyta.student@gmail.com',
                    password: 'ivrit007'
                },
                own: {
                    host: 'smtp.yandex.ru',
                    port: 587,
                    auth: {
                        user: 'main@sezam.shop',
                        pass: 'N0n3Fruit$?8+'
                    }
                }
            }
        }    
    
        let transporter = await nodemailer.createTransport({
            host: _credentials.services.own.host,
            port: _credentials.services.own.port,
            secure: false,
            auth: {
                user: _credentials.services.own.auth.user,
                pass: _credentials.services.own.auth.pass
            }
        })
    
        try {
            await transporter.sendMail({
                from: `"SezamGroup Bot" <${_credentials.services.own.auth.user}>`,
                to: `danielbyta.work@gmail.com`,
                subject: `no-reply | Сформирована одиночный заказ №${'NEED TO PASS PRODCUCT_ID'}`,
                text: '',
                html: `<p>${JSON.stringify(data, 0, 4)}</p>`
            }, (err) => {
                if (err) throw new Error('Не удалось отправить сформированную группу на почту')
            })
        } catch(e) {
            console.log(`!ERROR: ConfirmationManager, ${e.message}`, e)
        }
    }

    async function sendToTlgLogistics(data) {
        try {
            var response = await axios.post(`https://api.telegram.org/bot432467583:AAFynRLELNpSQ7VTFys8DberzRe82u933wM/sendMessage?chat_id=-1001391266350`, 
            {
                text: 
                `
==============================
Сформирован заказ №${data.order.order_id} на сумму ${Math.round(data.order.total)} рублей.
    Информация о заказе:
        - Order Unique Hash: ${data.order.order_unique_hash}
    Информация о платеже:
        - Amount: ${Number(data.tinkoff.Amount)}
        - CardId: ${data.tinkoff.CardId}
        - Pan: ${data.tinkoff.Pan}
        - PaymentId: ${data.tinkoff.PaymentId}
        - TerminalKey: ${data.tinkoff.TerminalKey}
        - Token: ${data.tinkoff.Token}
    Данные пользователя:
        - Фамилия/Имя: ${data.order.payment_lastname} ${data.order.payment_firstname}
        - Email: ${data.order.email}
        - Телефон: ${data.order.telephone}
    Доставка:
        - Способ доставки: ${data.order.shipping_method}
        - Адрес доставки: ${data.order.shipping_country}, ${data.order.shipping_city}, ${data.order.shipping_address_1}, ${data.order.shipping_postcode}
        - Стоимость доставки: ${data.order.totals[1].value}
${getBoxberryInfo(data.order.boxberry_address)}
    Товары: ${data.products.map(product => {
        return `\n
        - Database Group Trace Index: ${product.firebase.group_id}
        - Product Uniaue Hash: ${product.unique_hash}
        - Group Unique Hash: ${product.group_unique_hash}
        - ID бренда (opencart): ${product.manufacturer_id || product.brand_name_id}
        - Бренд (openart): ${product.model || product.brand_name}
        - Поставщик: ${product.name_legal || product.enterprise_name}
        - Адрес склада: ${product.address_storage}
        - ID товара: ${product.product_id}
        - Название товара: ${product.name}
        - Ссылка: ${product.href}
        - Тип товара: ${product.price_type}
        - Количество: ${product.quantity}
        - Групповая цена на момент закрытия: ${product.group_price_current}
        - Итог: ${product.group_price_current*product.quantity}
        - Нужно вернуть: ${getRefundAmount(product)}\n`
    })}
==============================`
            }
            )
            
        } catch(e) {
            throw new Error(`TELEGRAM_REQUEST_ERROR: ${e.message}`)
        }
        if (!response.data.ok) throw new Error(response.data.description)
    }

    console.log(`SEND ORDER INTERVAL INITIALIZED! NEW WAVE EVERY ${TIME/1000}s`)
    const sendOrderManager = setInterval(async () => {
        console.log('New wave of send order manager interval')
        let snapshot = await db
            .collection('opencart_order_buffer').doc('buffer')
            .collection('processing').get()
    
        let orderPromises = []
        snapshot.forEach(doc => {
            orderPromises.push(new Promise((resolve, rej) => resolve(doc.data())))
        })
        let orders = await Promise.all(orderPromises)
        for (let order of orders) {
            let edited = false
            let allProcessed = true
            for (let product of order.products) {
                let docId
                Boolean(product.group_unique_hash) ? 
                    docId = `group:product_id=${product.product_id}_hash=${product.group_unique_hash}` :
                    docId = `group:product_id=${product.product_id}_hash=${product.unique_hash}`
                let doc = await db.collection('opencart_order_groups').doc('groups').collection('success').doc(docId).get()
                let group = await doc.data()

                if (Boolean(group) && Boolean(group.is_processed)) {
                    product.is_processed = group.is_processed
                    product.firebase = {}
                    product.firebase.group_id = docId
                    product.firebase.quantity = group.current_quantity
                    product.firebase.members_count = group.current_group_members
                    product.firebase.filled_by_members = group.filled_by_members
                    product.firebase.filled_by_quantity = group.filled_by_quantity
                    // await db.collection('opencart_order_groups').doc('groups').collection('success').doc(docId).delete()
                    edited = true
                } else {
                    allProcessed = false
                }
            }
            if (allProcessed) {
                await db.collection('opencart_order_buffer').doc('buffer').collection('processing').doc(`order_${order.order.order_id}`).delete()
                await db.collection('opencart_order_buffer').doc('buffer').collection('processed').doc(`order_${order.order.order_id}`).set(order)
                console.log('SEND_ORDER_MANAGER: Processed order! Need to send it to sezam logistics.')
                await sendToTlgLogistics(order)
                // await sendOrderEmail(order)
            } else {
                if (edited) {
                    await db.collection('opencart_order_buffer').doc('buffer').collection('processing').doc(`order_${order.order.order_id}`).set(order)
                    console.log('SEND_ORDER_MANAGER: Updated info about order')
                }
            }
        }
    }, TIME)

    return sendOrderManager
}