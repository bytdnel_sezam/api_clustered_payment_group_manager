const _DEFAULT_TIME = 1000*60*15

async function sendSuccessGroupEmail(data) {
    const nodemailer = require('nodemailer');
    let _credentials = {
        services: {
            gmail: {
                itself: 'gmail',
                email: 'danielbyta.student@gmail.com',
                password: 'ivrit007'
            },
            own: {
                host: 'smtp.yandex.ru',
                port: 587,
                auth: {
                    user: 'main@sezam.shop',
                    pass: 'N0n3Fruit$?8+'
                }
            }
        }
    }    

    let transporter = await nodemailer.createTransport({
        host: _credentials.services.own.host,
        port: _credentials.services.own.port,
        secure: false,
        auth: {
            user: _credentials.services.own.auth.user,
            pass: _credentials.services.own.auth.pass
        }
    })

    try {
        await transporter.sendMail({
            from: `"SezamGroup Bot" <${_credentials.services.own.auth.user}>`,
            to: `vendor@sezam.shop`,
            subject: `no-reply | Сформирована группа по товару №${data.group_members[0].product_id}`,
            text: '',
            html: `<p>${JSON.stringify(data, 0, 4)}</p>`
        }, (err) => {
            if (err) throw new Error('Не удалось отправить сформированную группу на почту')
        })
    } catch(e) {
        console.log(`!ERROR: GroupsManager, ${e.message}`, e)
    }
}

module.exports = function(path, TIME = _DEFAULT_TIME, db) {
    console.log(`GROUP MANAGER INTERVAL INITIALIZED! NEW WAVE EVERY ${TIME/1000}s`)
    // START
    const GROUP_PATH = {
        CURRENT: `${path}/current`,
        CLOSED: {
            itself: `${path}/closed`,
            FAIL: `${path}/closed/fail`,
            SUCCESS: `${path}/closed/success`
        }
    }

    const groupManager = setInterval(async () => {
        console.log('New wave of group manager interval')
        let snapshot = await db
            .collection('opencart_order_groups').doc('groups')
            .collection('pending').get()
    
        let groupPromises = []
        snapshot.forEach(doc => {
            groupPromises.push(new Promise((resolve, rej) => {
                let data = doc.data()
                data.firebase_id = doc.id
                resolve(data)
            }))
        })

        let groups = await Promise.all(groupPromises)
        for (let group of groups) {
            let time = Date.now()
            if (group.date_expires <= time) {
                // time expired
                group.status = 'CLOSED_SUCCESS'
                group.comment = 'Created group because of enough product quantity after time expired!'
                group.is_processed = true
                // group
                await db.collection('opencart_order_groups').doc('groups').collection('pending').doc(group.firebase_id).delete()
                await db.collection('opencart_order_groups').doc('groups').collection('success').doc(`${group.firebase_id}_hash=${group.unique_hash}`).set(group)
            } else if (group.filled_by_members || group.filled_by_quantity) {
                // move to success groups
                group.status = 'CLOSED_SUCCESS'
                group.comment = 'Created group because of enough product quantity. It is ahead of time!'
                group.is_processed = true
                await db.collection('opencart_order_groups').doc('groups').collection('pending').doc(group.firebase_id).delete()
                await db.collection('opencart_order_groups').doc('groups').collection('success').doc(`${group.firebase_id}_hash=${group.unique_hash}`).set(group)
            }
        }


        
        /*for (let FILENAME of fs.readdirSync(GROUP_PATH.CURRENT)) {
            if (FILENAME !== 'zatichka.json') {
                let group = JSON.parse(fs.readFileSync(`${GROUP_PATH.CURRENT}/${FILENAME}`))

                if (group.date_expires <= time) {
                    // time expired
                    if (!group.filled_by_members && !group.filled_by_quantity) {
                        // move to fail groups
                        group.status = 'CLOSED_FAIL'
                        group.comment = 'Not enough members or product quantity. Failed to create group because of expiring time!'
                        fs.writeFileSync(`${GROUP_PATH.CURRENT}/${FILENAME}`, JSON.stringify(group, 0, 4))
                        fs.renameSync(`${GROUP_PATH.CURRENT}/${FILENAME}`, `${GROUP_PATH.CLOSED.FAIL}/${FILENAME}`)
                    } else {
                        // move to success
                        group.status = 'CLOSED_SUCCESS'
                        group.comment = 'Created group because of enough product quantity after time expired!'
                        fs.writeFileSync(`${GROUP_PATH.CURRENT}/${FILENAME}`, JSON.stringify(group, 0, 4))
                        fs.renameSync(`${GROUP_PATH.CURRENT}/${FILENAME}`, `${GROUP_PATH.CLOSED.SUCCESS}/${FILENAME}`)
                        await sendSuccessGroupEmail(group)
                    }
                } else {
                    // have time yet
                    if (group.filled_by_members || group.filled_by_quantity) {
                        // move to success groups
                        group.status = 'CLOSED_SUCCESS'
                        group.comment = 'Created group because of enough product quantity. It is ahead of time!'
                        fs.writeFileSync(`${GROUP_PATH.CURRENT}/${FILENAME}`, JSON.stringify(group, 0, 4))
                        fs.renameSync(`${GROUP_PATH.CURRENT}/${FILENAME}`, `${GROUP_PATH.CLOSED.SUCCESS}/${FILENAME}`)
                        await sendSuccessGroupEmail(group)
                    }
                }
            }
        }
        */
    }, TIME)
    return groupManager
}