const _DEFAULT_TIME = 1000*60*5
const crypto = require('crypto')

module.exports = function(path, TIME = _DEFAULT_TIME, db) {
    console.log(`CONFIRMATION_MANAGER INTERVAL INITIALIZED! NEW WAVE EVERY ${TIME/1000}s`)
    const BUFFER_PATH = {
        AUTHORIZED: `${path}/authorized`,
        NON_STATUS: `${path}/non_status`
    }

    const TYPES = {
        GROUP: 'group',
        SINGLE: 'single'
    }

    const getGroupDocName = (product_id) => `group:product_id=${product_id}`
    // const getHash = async () => crypto.randomBytes(25).toString('hex')
    const createGroup = async (order, {product, productIdx}) => {
        let groupDoc = getGroupDocName(product.product_id)
        let presentContinuousTime = Date.now()
        let ms_to_expire = (24*60*60*1000)
        let max_group_members = 10
        let max_product_quantity = 10
        let product_quantity = product.quantity
        let group_members = [
            {
                order_id: Number(order.order.order_id),
                firstname: order.order.firstname,
                lastname: order.order.lastname,
                email: order.order.email,
                telephone: order.order.telephone,
                group_price: Number(product.group_price),
                // group_price_current: Number(product.group_price_current),
                single_price: Number(product.single_price),
                product_id: Number(product.product_id),
                quantity: Number(product_quantity)
            }
        ]
        const groupData = {
            status: product.price_type == TYPES.GROUP ? 'PENDING' : 'CLOSED_SUCCESS',
            // date_created: Date.now(),
            price_type: product.price_type,
            single_price: Number(product.single_price),
            group_price_current: Number(product.group_price_current),
            group_price: Number(product.group_price),
            comment: product.price_type == TYPES.GROUP ? 'This group is just created!' : 'This is a single order closed with success status', 
            date_created: presentContinuousTime,
            date_expires: presentContinuousTime + ms_to_expire,
            ms_to_expire,
            max_group_members,
            max_product_quantity,
            current_group_members: group_members.length,
            filled_by_members: group_members.length < max_group_members ? false : true,
            filled_by_quantity: product_quantity < max_product_quantity ? false: true,
            current_quantity: 0 + Number(product_quantity),
            unique_hash: product.unique_hash,
            group_unique_hash: product.unique_hash,
            is_processed: product.is_processed,
            brand_name: product.model,
            enterprise_name: product.name_legal,
            brand_name_id: product.manufacturer_id,
            href: product.href,
            group_members
        }

        if (groupData.price_type == TYPES.GROUP) {
            await db.collection('opencart_order_groups').doc('groups').collection('pending').doc(groupDoc).set(groupData)
            let doc = await db.collection('opencart_order_buffer').doc('buffer').collection('processing').doc(`order_${order.order.order_id}`).get()
            let mainOrder = await doc.data()
            mainOrder.products[productIdx].group_unique_hash = groupData.group_unique_hash
            await db.collection('opencart_order_buffer').doc('buffer').collection('processing').doc(`order_${order.order.order_id}`).set(mainOrder)
            console.log('CONFIRMATION_MANAGER: new group initialized', groupDoc)
        } else if (groupData.price_type == TYPES.SINGLE) {
            groupData.is_processed = true
            await db.collection('opencart_order_groups').doc('groups').collection('success').doc(`${groupDoc}_hash=${groupData.group_unique_hash}`).set(groupData)
            console.log('CONFIRMATION_MANAGER: new single type group closed', groupDoc)
        }
    }

    const confirmationManager = setInterval(async () => {
        console.log('New wave of CONFIRMATION_manager interval')
        let snapshot = await db
            .collection('opencart_order_buffer').doc('buffer')
            .collection('authorized').get()
        
        let orderPromises = []
        snapshot.forEach(doc => orderPromises.push(new Promise((resolve, rej) => resolve(doc.data()))))

        let orders = await Promise.all(orderPromises)
        for (let order of orders) {
            await db.collection('opencart_order_buffer').doc('buffer').collection('authorized').doc(`order_${order.order.order_id}`).delete()
            await db.collection('opencart_order_buffer').doc('buffer').collection('processing').doc(`order_${order.order.order_id}`).set(order)
            console.log('CONFIRMATION_MANAGER: Order is processing. Start managing products!')
            // console.log(order)
            let productIdx = 0
            for (let product of order.products) {
                let groupDoc = getGroupDocName(product.product_id)
                let pending = await db.collection('opencart_order_groups').doc('groups').collection('pending').doc(groupDoc).get()
                let success = await db.collection('opencart_order_groups').doc('groups').collection('success').doc(groupDoc).get()
                let fail = await db.collection('opencart_order_groups').doc('groups').collection('fail').doc(groupDoc).get()
                let exists = Boolean(pending.data()) || Boolean(success.data()) || Boolean(fail.data())
                if (exists) {
                    // группа есть
                    let group = pending.data()
                    let isGroup = Boolean(group)
                    if (isGroup) {
                        console.log('group')
                        if (!group.filled_by_members && !group.filled_by_quantity) {
                            const member = {
                                order_id: Number(order.order.order_id),
                                firstname: order.order.firstname,
                                lastname: order.order.lastname,
                                email: order.order.email,
                                telephone: order.order.telephone,
                                group_price: Number(product.group_price),
                                group_price_current: Number(product.group_price_current),
                                single_price: Number(product.single_price),
                                product_id: Number(product.product_id),
                                quantity: Number(product.quantity)
                            }
                            group.group_members.push(member)
                            group.comment = 'New group member!'
                            group.current_quantity += Number(product.quantity)
                            group.current_group_members = group.group_members.length
                            group.filled_by_quantity = group.current_quantity < group.max_product_quantity ? false : true
                            group.filled_by_members = group.group_members.length < group.max_group_members ? false : true
                            await db.collection('opencart_order_groups').doc('groups').collection('pending').doc(groupDoc).set(group)
                            product.group_unique_hash = group.group_unique_hash
                            await db.collection('opencart_order_buffer').doc('buffer').collection('processing').doc(`order_${order.order.order_id}`).set(order)
                            console.log('CONFIRMATION_MANAGER: new group member')
                        } else {
                            console.log('CONFIRMATION_MANAGER: This group does not have empty place to join! Need to close group and return money.', groupDoc)
                            await db.collection('opencart_order_groups').doc('groups').collection('pending').doc(groupDoc).delete()
                            group.status = 'CLOSED_SUCCESS'
                            group.is_processed = true

                            await db.collection('opencart_order_groups').doc('groups').collection('success').doc(`${groupDoc}_hash=${group.group_unique_hash}`).set(group)
                            let doc = await db.collection('opencart_order_buffer').doc('buffer').collection('processing').doc(`order_${order.order.order_id}`).get()
                            let mainOrder = await doc.data()
                            mainOrder.products[productIdx].group_unique_hash = groupData.group_unique_hash
                            await db.collection('opencart_order_buffer').doc('buffer').collection('processing').doc(`order_${order.order.order_id}`).set(mainOrder)
                            console.log('CONFIRMATION_MANAGER: group closed', groupDoc)
                            await createGroup(order, product)
                            console.log('CONFIRMATION_MANAGER: new group', groupDoc)
                        }
                    }
                } else {
                    // группы нет
                    await createGroup(order, {product, productIdx})
                }
                productIdx++
            }
        }
    }, TIME)

    return confirmationManager
}

async function sendSuccessSingleEmail(data) {
    const nodemailer = require('nodemailer');
    let _credentials = {
        services: {
            gmail: {
                itself: 'gmail',
                email: 'danielbyta.student@gmail.com',
                password: 'ivrit007'
            },
            own: {
                host: 'smtp.yandex.ru',
                port: 587,
                auth: {
                    user: 'main@sezam.shop',
                    pass: 'N0n3Fruit$?8+'
                }
            }
        }
    }    

    let transporter = await nodemailer.createTransport({
        host: _credentials.services.own.host,
        port: _credentials.services.own.port,
        secure: false,
        auth: {
            user: _credentials.services.own.auth.user,
            pass: _credentials.services.own.auth.pass
        }
    })

    try {
        await transporter.sendMail({
            from: `"SezamGroup Bot" <${_credentials.services.own.auth.user}>`,
            to: `vendor@sezam.shop`,
            subject: `no-reply | Сформирована одиночный заказ №${'NEED TO PASS PRODCUCT_ID'}`,
            text: '',
            html: `<p>${JSON.stringify(data, 0, 4)}</p>`
        }, (err) => {
            if (err) throw new Error('Не удалось отправить сформированную группу на почту')
        })
    } catch(e) {
        console.log(`!ERROR: ConfirmationManager, ${e.message}`, e)
    }
}
/*
for (let FILENAME of fs.readdirSync(BUFFER_PATH.AUTHORIZED)) {
    if (FILENAME !== 'zatichka.json') {
        let orderFile = JSON.parse(fs.readFileSync(`${BUFFER_PATH.AUTHORIZED}/${FILENAME}`))
        let groupProducts = []
        let singleProducts = []
        async function groupFactory() {
            for (let product of orderFile.products) {
                if (product.price_type == TYPES.GROUP) {
                    // group product in order
                    await groupProducts.push(product)
                } else if (product.price_type == TYPES.SINGLE) {
                    // single product in order
                    await singleProducts.push(product)
                }
            }
        }
        groupFactory().then(async () => {
            if (groupProducts.length > 0) {
                for (let groupProduct of groupProducts) {
                    let groupFileDir = `./groups/current/opencart_product_${groupProduct.product_id}_group.json`

                    function createNewGroup(product, groupFileDir) {
                        let presentContinuousTime = Date.now()
                        let ms_to_expire = (24*60*60*1000)
                        let max_group_members = 10
                        let max_product_quantity = 10
                        let product_quantity = product.quantity
                        let group_members = [
                            {
                                order_id: orderFile.order.order_id,
                                firstname: orderFile.order.firstname,
                                lastname: orderFile.order.lastname,
                                email: orderFile.order.email,
                                telephone: orderFile.order.telephone,
                                group_price: Number(product.group_price),
                                product_id: product.product_id,
                                quantity: product_quantity
                            }
                        ]
                        const groupData = {
                            status: 'CURRENT',
                            comment: 'This group is just created!',
                            date_created: presentContinuousTime,
                            date_expires: presentContinuousTime + ms_to_expire,
                            ms_to_expire,
                            max_group_members,
                            max_product_quantity,
                            current_group_members: group_members.length,
                            filled_by_members: group_members.length < max_group_members ? false : true,
                            filled_by_quantity: product_quantity < max_product_quantity ? false: true,
                            current_quantity: 0 + Number(product_quantity),
                            group_members
                        }
                        fs.writeFileSync(groupFileDir, JSON.stringify(groupData, 0, 4))
                    }
                
                    function addGroupMember({groupProduct, order}, groupFileDir) {
                        let group = JSON.parse(fs.readFileSync(groupFileDir))
                        const member = {
                            order_id: order.order_id,
                            firstname: order.firstname,
                            lastname: order.lastname,
                            email: order.email,
                            telephone: order.telephone,
                            group_price: Number(groupProduct.group_price),
                            product_id: groupProduct.product_id,
                            quantity: groupProduct.quantity
                        }
                        if (!group.filled_by_members && !group.filled_by_quantity) {
                            group.group_members.push(member)
                            group.comment = 'New group member!'
                            group.current_quantity += Number(groupProduct.quantity)
                            group.current_group_members = group.group_members.length
                            group.filled_by_quantity = group.current_quantity < group.max_product_quantity ? false : true
                            group.filled_by_members = group.group_members.length < group.max_group_members ? false : true
                    
                            fs.writeFileSync(groupFileDir, JSON.stringify(group, 0, 4))
                        } else {
                            console.log('This group does not have empty place to join! Need to close group and return money.')
                        }
                    }

                    try {
                        // group already exists
                        fs.readFileSync(groupFileDir)
                        await addGroupMember(
                            {
                                groupProduct, 
                                order: orderFile.order
                            }, groupFileDir
                        )
                    } catch(e) {
                        // need to create new group
                        await createNewGroup(groupProduct, groupFileDir)
                    }
                }
            }

            if (singleProducts.length > 0) {
                let data = {
                    tinkoff: orderFile.tinkoff,
                    order: orderFile.order,
                    products: singleProducts
                }
                fs.writeFileSync(
                    `./single/single_order_${orderFile.order.order_id}.json`, 
                    JSON.stringify(data, 0, 4)
                )
                // send success single_price order to email
                await sendSuccessSingleEmail(data)
            }
        }) 
        fs.unlinkSync(`${BUFFER_PATH.AUTHORIZED}/${FILENAME}`)
    }
} */